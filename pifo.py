#!/usr/bin/env python3

from urllib import request
from HTMLParser import HTMLParser
from irc.bot import SingleServerIRCBot
from code import InteractiveInterpreter
from io import StringIO
from multiprocessing import Process, Queue
from queue import Empty
from queue import LifoQueue

import irc
import signal
import sys
import random
import cleverbot3


SERVER = 'irc.rezosup.org'
PORT = 6667
PASSWORD = ''
KADOC = []


class Pafo(SingleServerIRCBot, HTMLParser):

    def __init__(self, nick='pifo', list_chans=None):
        self.todo_dict = {}
        self.whitelist = ['corwin']
        self.q = Queue()
        self.pifo_queue = Queue()
        self.lifo = LifoQueue()
        self.padlist = {}
        self.in_link = False
        self.home = 'Corwin'
        self.nick = nick
        self.interpreter = InteractiveInterpreter()
        self.list_chans = list_chans if list_chans else []
        SingleServerIRCBot.__init__(self, ([(SERVER, PORT)]), self.nick,
                                    self.nick, reconnection_interval=1)
        HTMLParser.__init__(self)
        self.cl = cleverbot3.Cleverbot()

    def on_welcome(self, server, event):
        server.buffer_class = irc.buffer.LenientDecodingLineBuffer
        server.buffer_class.encoding = 'latin1'
        server.buffer_class.errors = 'replace'
        try:
            server.privmsg('chanserv', 'identify ' + PASSWORD)
        except:
            server.privmsg('chanserv', ('identify ' +
                           PASSWORD).encode('latin1'))
        for chan in self.list_chans:
            server.join(chan)
        server.buffer_class.encoding = 'utf-8'

    def on_privmsg(self, server, event):
        self.process_msg(server, event)

    def on_pubmsg(self, server, event):
        self.process_msg(server, event)

    def process_msg(self, server, event):
        self.nick = server.get_nickname()
        source = event.source.split('!')[0]
        target = source if event.target == self.nick else event.target
        cmd = event.arguments[0].split()
        self.cleversend(server, target, cmd)
        if len(cmd) == 0:
            return
        rnd = random.randint(0, 1)
        if rnd == 1:
            if 'bruce' in source.lower():
                server.kick(event.target, source, "nope")
            else:
                for w in cmd:
                    if 'bruce' in w.lower().replace(':', ''):
                        server.kick(event.target, w, "nope")
        if len(cmd) == 1 and cmd[0] == '.':
            server.privmsg(target, 'Fetching messages...')
            return
        for word in cmd:
            pos = word.lower().find('di')
            if pos != -1:
                self.di_something(server, source, target, word[pos + 2:])
            else:
                pos = word.lower().find('dy')
                if pos != -1:
                    self.di_something(server, source, target, word[pos + 2:])
        for word in cmd:
            pos = word.find('cri')
            if pos != -1:
                self.cri_something(server, source, target, word[pos + 3:])
        if (len(cmd) >= 2 and (cmd[0] == self.nick + ':' or cmd[0] == self.nick + ',')
           and cmd[1][0] == '!'):
            if (cmd[1] == '!join' or cmd[1] == '!leave' or cmd[1] ==
                    '!whitelist' or cmd[1] == '!python') and source != 'Corwin':
                return
            if not source.lower() in self.whitelist:
                server.privmsg(target if target[0] == '#'
                            else source, 'nope')
                return
            self.execute(server, ssourcee, target, cmd[1:])
        elif self.nick in cmd:
            if('bmoc' in source.lower()):
                server.privmsg(target, source + ': Source ?')
            elif ('dettorer' in source.lower()):
                server.privmsg(target, "Tu veux un bol de lait " + source +
                                " ?")
            else:
                server.privmsg(target, "D'où tu me hl " +
                                source + '?')
        else:
            if cmd[0][0] == '!':
                if ((cmd[0] == '!join' or cmd[0] ==
                    '!python' or cmd[0] == '!leave' or cmd[0] == '!whitelist') and source != 'Corwin'):
                    return
                if not source.lower() in self.whitelist:
                    server.privmsg(target if target[0] == '#'
                                else source, 'nope')
                    return
                if cmd[0] == '!padlist' or cmd == '!help':
                    self.execute(server, source, target, cmd)
                else:
                    self.execute(server, source, target, cmd)

    def cleverbloated(self, line, q):
        message = self.cl.ask(line)
        q.put(message.strip())

    def cleversend(self, server, target, cmd):
        try:
            p = Process(target=self.cleverbloated, args=(cmd, self.q))
            p.start()
            p.join()
            if p.is_alive():
                p.terminate()
            try:
                message = self.q.get(False)
                server.privmsg(target, message)
            except Empty as e:
                print(sys.exc_info()[0])
                print(e)
        except:
            pass

    def execute(self, server, source, target, command):
        if command[0][0] == '!':
            command[0] = command[0][1:]
            if command[0] == 'whitelist':
                self.exec_whitelist(server, source, command[1:])
                return
        try:
            method = getattr(self, 'exec_' + command[0])
            if method == self.exec_python:
                p = Process(target=method, args=(server, source, target, command[1:],
                    self.q))
                p.start()
                p.join(3)
                if p.is_alive():
                    p.terminate()
                try:
                    message = self.q.get(False)
                    server.privmsg(target, message)
                except Empty as e:
                    print(sys.exc_info()[0])
                    print(e)
            else:
                method(server, source, target, command[1:])
        except Exception as e:
            print(sys.exc_info()[0])
            server.privmsg(target, 'Nope, so much nope.')

    def exec_whitelist(self, server, source, target, command):
        if len(command) == 0:
            return
        if len(command) == 1 and command[0] == 'list':
            server.privmsg(source, 'Enabled users: ' + ' '.join(self.whitelist))
        elif len(command) > 1:
            if command[0] == 'add':
                for name in command[1:]:
                    self.whitelist.append(name.lower())
            if command[0] == 'del':
                for name in command[1:]:
                    self.whitelist.remove(name.lower())
            return

    def exec_join(self, server, source, target, command):
        if len(command) < 1:
            server.privmsg(source,'Hey ho, pour qui tu te prends ?')
            return
        for cmd in command:
            if cmd[0] == '#':
                self.list_chans.append(cmd)
                server.join(cmd)
            else:
                server.privmsg(target,'Donne un chan bâtard')
                return

    def exec_leave(self, server, source, target, command):
        if len(command) < 1:
            server.privmsg(target,'Hey ho, pour qui tu te prends ?')
            return
        for cmd in command:
            if cmd[0] == '#':
                self.list_chans.append(cmd)
                server.part(cmd)
            else:
                server.privmsg(target,'Donne un chan bâtard')
                return

    def exec_kick(self, server, source, target, command):
        server.kick(target, command[0], ' '.join(command[1:]))

    def exec_tg(self, server, source, target, command):
        server.privmsg(target,'Ta gueule ' + (command[0] if len(command) > 0 and command[0] !=
                               'Corwin' else 'Bruce') + ' !')
        return

    def exec_query(self, server, source, target, command):
        if len(command) < 2:
            return 'File moi un message bordel.'
        else:
            message = ''
            for s in command[1:]:
                message += s + ' '
            if command[0] != 'toogy' or command[0] != '#gconfs':
                try:
                    server.privmsg(command[0], message)
                except:
                    server.privmsg(command[0], message.encode('latin1'))
            return

    def exec_home(self, server, source, target, command):
        server.privmsg(target, 'J\'habite chez ' + self.home)
        return

    def exec_move(self, server, source, target, command):
        if len(command) < 1:
            server.privmsg(source, 'Tu me prends pour un SDF ? Donne moi une maison wesh !')
            return
        else:
            self.home = command[0]

    def exec_help(self, server, source, target, command):
        server.privmsg(target, '!join #canal_1 [#canal_2 #canal_3...] -> join the channels.')
        server.privmsg(target, '!tg [user] -> say \'Ta gueule [user | Bruce]\'.')
        server.privmsg(target, '!query user -> send a query to the specified user.')
        server.privmsg(target, '!home -> say \'J\'habite chez [Corwin | user]\'.')
        server.privmsg(target, '!move user -> move the bot\'s home.')
        server.privmsg(target, '!help -> display this message. Splays.')
        server.privmsg(target, '!kadoc [user] -> send a quote of Kadoc to the user if provided, or the current channel by default.')
        server.privmsg(target, '!padlist -> display the list of pads from pad.gconfs.fr in a query')
        server.privmsg(target, '!padlink [user] padname -> display the link of the pad from pad.gconfs.fr in a query to the user if provided.')
        server.privmsg(target, '!updatelist -> update the list of pads from pad.gconfs.fr.')
        server.privmsg(target, '!python command -> execute command and print its output to the current channel')

    def exec_kadoc(self, server, source, target, command):
        if target == '#gconfs':
            return
        pos = random.randint(0, len(KADOC) -1)
        if len(command) == 0:
            try:
                server.privmsg(target, KADOC[pos] + ' (Kadoc)')
            except:
                server.privmsg(target, (KADOC[pos] +
                               '(Kadoc)').encode('latin1'))
        else:
            if command[0] == '#gconfs':
                return
            try:
                server.privmsg(command[0], KADOC[pos] + '(Kadoc)')
            except:
                server.privmsg(command[0], (KADOC[pos] +
                               '(Kadoc)').encode('latin1'))

    def di_something(self, server, source, target, word):
        if target != '#gconfs':
            server.privmsg(target, word)

    def cri_something(self, server, source, target, word):
        if target != '#gconfs':
            server.privmsg(target, word.upper())

    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            self.in_link = True
        else:
            self.in_link = False

    def handle_endtag(self, tag):
        self.in_link = False

    def handle_data(self, data):
        if self.in_link:
            self.padlist[data] = 'http://pad.gconfs.fr/p/' + data

    def exec_updatelist(self, server, source, target, command):
        response = request.urlopen('http://pad.gconfs.fr/list')
        big_data = response.read().decode('utf-8')
        self.feed(big_data)
        return

    def exec_padlist(self, server, source, target, command):
        if not self.padlist:
            self.exec_updatelist(server, source, target, command, queue)
        for (pad,link) in self.padlist.items():
            server.privmsg(target, pad + ' : ' + link)
        return

    def exec_padlink(self, server, source, target, command):
        if not self.padlist:
            self.exec_updatelist(server, source, target, command, queue)
        k = ''
        keys = []
        if len(command) >= 2:
            k = command[1]
        else:
            k = command[0]
        for key in self.padlist.keys():
            if key.find(k) != -1:
                keys.append(key)
        if keys:
            for key in keys:
                server.privmsg(command[0] if (len(command) >= 2) else target,
                        self.padlist[key])
        else:
            server.privmsg(source, 'This pad is not in my list. You can try to update me with !updatelist')

    def exec_python(self, server, source, target, command, queue):
        if target == '#gconfs':
            return
        buff = ' '.join(command)
        try:
            obj = compile(buff, '<string>', 'exec')
            with Capturing() as output:
                self.interpreter.runcode(obj)
            for line in output:
                server.privmsg(target, line)
        except SyntaxError:
            print(sys.exc_info()[0])
            server.privmsg(target, 'I don\'t understand')
        except:
            print(sys.exc_info()[0])

    def exec_nick(self, server, source, target, command):
        if len(command) == 0:
            server.privmsg(target,'Give me a name, bro !')
        print(command[0])
        self.nick = command[0]
        server.nick(self.nick)
        print(self.nick)

    def exec_enqueue(self, server, source, target, command):
        message = ' '.join(command)
        self.pifo_queue.put(message, block=False)
        return

    def exec_dequeue(self, server, source, target, command):
        try:
            message = self.pifo_queue.get(False)
            server.privmsg(target,message)
        except Empty:
            server.privmsg(target,"Oh stop it you !")

    def exec_todo(self, server, source, target, command):
        if command[0] == 'add':
            try:
                self.todo_dict[source].append(' '.join(command[1:]))
            except:
                self.todo_dict[source] = []
                self.todo_dict[source].append(' '.join(command[1:]))
            return
        elif command[0] == 'done':
            try:
                i = int(command[1]) - 1
            except:
                server.privmsg(target, "Tocard")
            if len(self.todo_dict[source]) < i:
                server.privmsg(target, "Tocard")
            task = self.todo_dict[source].pop(i)
            server.privmsg(target, '[{}] {}'.format(i, task))
        elif command[0] == 'rm':
            try:
                i = int(command[1]) - 1
            except:
                server.privmsg(target, "Tocard")
            if len(self.todo_dict[source]) < i:
                server.privmsg(target, "Tocard")
            try:
                self.todo_dict[source].pop(i)
            except:
                server.privmsg(target, "{}: Your todo list is "
                        "empty.".format(source))
        elif command[0] == 'list':
            try:
                for i in range(len(self.todo_dict[source])):
                    server.privmsg(target, '{}: {}'.format(i+1, self.todo_dict[source][i]))
            except Exception as e:
                print("{} : {} ".format(sys.exc_info()[0], e))
                server.privmsg(target, "{}: Your todo list is "
                        "empty.".format(source))

class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self
    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        sys.stdout = self._stdout



if __name__ == '__main__':
    with open('kadoc', 'r') as f:
        for line in f:
            KADOC.append(line.rstrip('\n'))
    script, parameters = sys.argv
    with open(parameters, 'r') as f:
        parameters = f.readlines()
    nick = parameters[0].strip('\n')
    PASSWORD = parameters[1].strip('\n')
    chans = parameters[2].strip('\n').split()
    pafo = Pafo(nick=nick, list_chans=chans)
    pafo.start()
