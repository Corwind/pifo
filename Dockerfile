FROM python:3-onbuild

#RUN  yes Y | apt-get update

RUN sed -i s/markupbase/_markupbase/g /usr/local/lib/python3.4/site-packages/HTMLParser.py
RUN sed -i s/DecodingLineBuffer/LenientDecodingLineBuffer/g /usr/local/lib/python3.4/site-packages/irc/client.py
RUN git clone https://github.com/Corwind/cleverbot3.py.git && cd cleverbot3.py && pip3 install .
CMD ["/usr/bin/bash", "/root/pifolaunch.sh"]
