#!/usr/bin/zsh
docker build -t pifo-img .
docker stop -t 0 pifo
docker rm pifo
docker run -d -m=100M --name=pifo -it pifo-img /bin/bash
